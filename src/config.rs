use crate::F;

use self::newtypes::{Ds, Length, MaxCurve, MaxDCurve, MaxDTorsion, MaxTorsion, NKeyPoint};

pub mod newtypes;

#[derive(Debug, Default)]
pub struct Config {
    // todo: remove most of it, add n_keypoint?
    pub length: F,
    pub ds: F,
    pub max_curv: F,
    pub max_tors: F,
    max_dcurv: F,
    max_dtors: F,
}

// #[derive(Debug)]
struct ConfigPoint {
    length: Length,
    ds: Ds,
    pub max_curv: MaxCurve,
    pub max_tors: MaxTorsion,
}

fn compute_ds(length: Length, n_kp: NKeyPoint, ds_desired: Ds) -> F {
    let l_seg = length.into_inner() / n_kp.0 as F;
    let n_per_seg = (l_seg / ds_desired.into_inner()).ceil();
    l_seg / n_per_seg
}

impl Config {
    pub fn new(
        length: Length,
        ds: Ds,
        max_curv: MaxCurve,
        max_tors: MaxTorsion,
        max_dcurv: MaxDCurve,
        max_dtors: MaxDTorsion,
    ) -> Self {
        Config {
            length: length.into_inner(),
            ds: ds.into_inner(),
            max_curv: max_curv.into_inner(),
            max_tors: max_tors.into_inner(),
            max_dcurv: max_dcurv.into_inner(),
            max_dtors: max_dtors.into_inner(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn compute_ds_round() {
        assert_eq!(
            compute_ds(
                Length::new(100.).unwrap(),
                NKeyPoint(10),
                Ds::new(0.1).unwrap()
            ),
            0.1
        );
    }

    #[test]
    fn compute_ds_approx() {
        assert_eq!(
            compute_ds(
                Length::new(100.).unwrap(),
                NKeyPoint(10),
                Ds::new(3.0).unwrap()
            ),
            2.5
        );
    }
}
