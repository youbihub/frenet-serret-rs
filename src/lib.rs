extern crate nalgebra as na;

use na::{
    Isometry3, Matrix, Rotation3, SliceStorage, SliceStorageMut, Storage, Translation3,
    UnitQuaternion, Vector, U1, U16, U3,
};
use ode_solvers::dop_shared::{IntegrationError, Stats};
use ode_solvers::dopri5::*;
use ode_solvers::*;

pub mod pcf;

type State = SVector<f64, 16>; //RTNBktss'
type Time = f64;

pub struct FS<F, G, H>
where
    F: Fn(f64) -> f64,
    G: Fn(f64) -> f64,
    H: Fn(f64) -> f64,
{
    y0: State, //todo: maybe keep all private + build ctor
    dynamics: Dynamics<F, G, H>,
    // pub length: f64,
    solver_params: DopriParams,
}

impl<F, G, H> FS<F, G, H>
where
    F: Fn(f64) -> f64,
    G: Fn(f64) -> f64,
    H: Fn(f64) -> f64,
{
    pub fn build(
        dynamics: Dynamics<F, G, H>,
        y0: State,
        solver_params: DopriParams,
    ) -> Option<Self> {
        (acc(
            get::curv(&y0),
            get::n(&y0),
            get::ds_dt(&y0),
            get::t(&y0),
            (dynamics.dvel_ds)(0.),
        )
        .norm()
            <= dynamics.param.max_acc)
            .then_some(Self {
                y0,
                dynamics,
                solver_params,
            })
    }
}

pub fn new_init(curv_0: f64, tors_0: f64, speed_0: f64) -> State {
    //todo: bound newtypes? also do check for validity
    let mut y = State::zeros();
    let _r = y.fixed_rows_mut::<3>(0);
    let mut t = y.fixed_rows_mut::<3>(3);
    t.copy_from(&Vector3::<f64>::x());
    let mut n = y.fixed_rows_mut::<3>(6);
    n.copy_from(&Vector3::<f64>::y());
    let mut b = y.fixed_rows_mut::<3>(9);
    b.copy_from(&Vector3::<f64>::z());
    y[12] = curv_0;
    y[13] = tors_0;
    y[15] = speed_0;
    y
}

#[cfg(test)]
mod tests {
    use super::*;
    use na::SVector;
    #[test]
    fn test_y0() {
        let y0 = new_init(1.1, 2.2, 3.3);
        // r,t,n,b,k,t,s,ds
        // let xxx =      SVector::<f64, 16>::new(1., 2., 3., 4.);
        let expected = SVector::<f64, 16>::from_column_slice(&[
            /* r: */ 0., 0., 0., /* t: */ 1., 0., 0., /* n: */ 0., 1., 0.,
            /* b: */ 0., 0., 1., /* curv: */ 1.1, /* tors:  */ 2.2,
            /* s: */ 0., /* ds/dt */ 3.3,
        ]);
        assert_eq!(y0, expected);
    }
}

pub struct Dynamics<F, G, H>
where
    F: Fn(f64) -> f64,
    G: Fn(f64) -> f64,
    H: Fn(f64) -> f64,
{
    pub dcurv_ds: F,
    pub dtors_ds: G,
    pub dvel_ds: H,

    pub param: DynamicsParam,
}

pub struct DynamicsParam {
    pub max_curve: f64,
    pub max_tors: f64,
    pub max_vel: f64,
    pub max_acc: f64,
    pub max_length: f64,
}

pub struct DopriParams {
    pub dt: f64,
    pub max_time: f64,
    pub rtol: f64,
    pub atol: f64,
}

fn cap_derivative_if_max(max_value: f64, value: f64, dvalue: f64, should_cap: bool) -> f64 {
    match (value, should_cap) {
        (_, true) if value > max_value => dvalue.min(0.),
        (_, true) if value < -max_value => dvalue.max(0.),
        (_, _) => dvalue,
    }
}

type V3Slice<'a> = Matrix<f64, U3, U1, SliceStorage<'a, f64, U3, U1, U1, U16>>;
type V3SliceMut<'a> = Matrix<f64, U3, U1, SliceStorageMut<'a, f64, U3, U1, U1, U16>>;

fn acc(curv: f64, n: V3Slice, v: f64, t: V3Slice, a: f64) -> Vector3<f64> {
    // !todo: could simplify using State for curv/n/v/t
    // P = R
    // V = dR/dt   = dR/ds*ds/dt
    //   = T*ds/dt = T*v
    // A = dV/dt   = d(T*v)/dt = dT/dt*v+T*a = dT/ds*ds/dt*v+T*a
    //   = curv*N *v^2+T*a
    curv * n * v.powi(2) + t * a + 9.81 * Vector3::z()
}
mod get {
    use crate::V3Slice;
    use nalgebra::SVector;

    // pub fn r(y: &SVector<f64, 16>) -> V3Slice {
    //     y.fixed_rows::<3>(0)
    // }
    pub fn t(y: &SVector<f64, 16>) -> V3Slice {
        y.fixed_rows::<3>(3)
    }
    pub fn n(y: &SVector<f64, 16>) -> V3Slice {
        y.fixed_rows::<3>(6)
    }
    pub fn b(y: &SVector<f64, 16>) -> V3Slice {
        y.fixed_rows::<3>(9)
    }
    pub fn curv(y: &SVector<f64, 16>) -> f64 {
        y[12]
    }
    pub fn tors(y: &SVector<f64, 16>) -> f64 {
        y[13]
    }
    pub fn s(y: &SVector<f64, 16>) -> f64 {
        y[14]
    }
    pub fn ds_dt(y: &SVector<f64, 16>) -> f64 {
        y[15]
    }
    pub mod mutable {
        use crate::V3SliceMut;
        use nalgebra::SVector;

        pub fn r(y: &mut SVector<f64, 16>) -> V3SliceMut {
            y.fixed_rows_mut::<3>(0)
        }
        pub fn t(y: &mut SVector<f64, 16>) -> V3SliceMut {
            y.fixed_rows_mut::<3>(3)
        }
        pub fn n(y: &mut SVector<f64, 16>) -> V3SliceMut {
            y.fixed_rows_mut::<3>(6)
        }
        pub fn b(y: &mut SVector<f64, 16>) -> V3SliceMut {
            y.fixed_rows_mut::<3>(9)
        }
        pub fn curv(y: &mut SVector<f64, 16>) -> &mut f64 {
            &mut y[12]
        }
        pub fn tors(y: &mut SVector<f64, 16>) -> &mut f64 {
            &mut y[13]
        }
        pub fn s(y: &mut SVector<f64, 16>) -> &mut f64 {
            &mut y[14]
        }
        pub fn ds_dt(y: &mut SVector<f64, 16>) -> &mut f64 {
            &mut y[15]
        }
    }
}
impl<F, G, H> ode_solvers::System<State> for Dynamics<F, G, H>
where
    F: Fn(f64) -> f64,
    G: Fn(f64) -> f64,
    H: Fn(f64) -> f64,
{
    // Equations of motion of the system
    fn system(&self, _time: Time, y: &State, dy: &mut State) {
        // use get::*;
        // does not work in functions, due to conflict in nalgebra versions
        // fn b(y: &State) -> XXX {
        //      y.fixed_rows::<3>(9)
        // }

        // let _r = y.fixed_rows::<3>(0);
        // let t = y.fixed_rows::<3>(3);
        let t = get::t(y);
        let n = get::n(y);
        let b = get::b(y);
        let curv = get::curv(y);
        let tors = get::tors(y);
        let s = get::s(y);
        let ds_dt = get::ds_dt(y);

        let acc_lin = (self.dvel_ds)(s);
        let acc_total = acc(curv, n, ds_dt, t, acc_lin);
        let should_cap = acc_total.norm() > self.param.max_acc;

        let mut dr_dt = get::mutable::r(dy);
        let dr_ds = &t;
        dr_dt.copy_from(&(dr_ds * ds_dt));

        let mut dt_dt = get::mutable::t(dy); //todo: use operation 0+3 and track idx
        let dt_ds = curv * n;
        dt_dt.copy_from(&(dt_ds * ds_dt));

        let mut dn_dt = get::mutable::n(dy);
        let dn_ds = -curv * t + tors * b;
        dn_dt.copy_from(&(dn_ds * ds_dt));

        let mut db_dt = get::mutable::b(dy);
        let db_ds = -tors * n;
        db_dt.copy_from(&(db_ds * ds_dt));

        let dcurv = get::mutable::curv(dy);
        *dcurv = (self.dcurv_ds)(s);

        *dcurv = cap_derivative_if_max(self.param.max_curve, curv, *dcurv, should_cap);

        let dtors = get::mutable::tors(dy);
        *dtors = (self.dtors_ds)(s);
        *dtors = cap_derivative_if_max(self.param.max_tors, tors, *dtors, should_cap);

        let vel = get::mutable::s(dy);
        *vel = ds_dt;

        let acc = get::mutable::ds_dt(dy);
        *acc = (self.dvel_ds)(s);
        *acc = cap_derivative_if_max(self.param.max_vel, ds_dt, *acc, should_cap);
    }
    fn solout(&mut self, _x: f64, y: &State, _dy: &State) -> bool {
        get::s(y) > self.param.max_length
    }
}

pub struct SolveOut
//<F, G, H>
where
// F: Fn(f64) -> f64,
// G: Fn(f64) -> f64,
// H: Fn(f64) -> f64,
{
    pub stats: Stats,
    // pub stepper: ode_solvers::Dopri5<
    //     Matrix<f64, Const<16>, Const<1>, ArrayStorage<f64, 16, 1>>,
    //     Dynamics<F, G, H>,
    // >,
    pub time: Vec<f64>,
    pub frenet_serret: Vec<Isometry3<f64>>,
    pub acc_aligned: Vec<Isometry3<f64>>,
}

type SolveResult = Result<SolveOut, IntegrationError>;

fn quat<S1, S2, S3>(
    t: &Vector<f64, U3, S1>,
    n: &Vector<f64, U3, S2>,
    _b: &Vector<f64, U3, S3>,
) -> UnitQuaternion<f64>
where
    S1: Storage<f64, U3>,
    S2: Storage<f64, U3>,
    S3: Storage<f64, U3>,
{
    // todo: compare without normalization. probably this does nothing notable?
    // todo: compare speed gains against Rotation3::from_matrix/Rotation3::from_matrix_eps(m)
    let t = t.normalize();
    let n = (n - (n.dot(&t)) * t).normalize();
    let b = t.cross(&n);
    UnitQuaternion::from_rotation_matrix(&Rotation3::from_basis_unchecked(&[t, n, b]))
}

pub fn solve<F, G, H>(system: FS<F, G, H>) -> SolveResult
where
    F: Fn(f64) -> f64,
    G: Fn(f64) -> f64,
    H: Fn(f64) -> f64,
{
    let mut stepper = Dopri5::new(
        system.dynamics,
        0.0,
        system.solver_params.max_time,
        system.solver_params.dt,
        system.y0,
        system.solver_params.rtol,
        system.solver_params.rtol,
    );
    match stepper.integrate() {
        //todo: use "?"
        Ok(res) => {
            let frenet_serret = stepper
                .y_out()
                .iter()
                .map(|y| {
                    //todo: use get::n(y) etc
                    let t = y.fixed_rows::<3>(3);
                    let n = y.fixed_rows::<3>(6);
                    let b = y.fixed_rows::<3>(9);
                    let mut iso = Isometry3::identity();
                    iso.rotation = quat(&t, &n, &b);
                    iso.translation = Translation3::new(y[0], y[1], y[2]);
                    iso
                })
                .collect();
            let acc_aligned = stepper
                .y_out()
                .iter()
                .map(|y| {
                    //todo: use get::n(y) etc
                    let mut iso = Isometry3::identity();
                    iso.translation = Translation3::new(y[0], y[1], y[2]);
                    let t = y.fixed_rows::<3>(3);
                    let n = y.fixed_rows::<3>(6);
                    let b = y.fixed_rows::<3>(9);
                    let curv = y[12];
                    let _tors = y[13];
                    let v = y[15];
                    let accel = acc(curv, n, v, t, 0.);
                    let left = t.cross(&accel).normalize();
                    iso.rotation = quat(&t, &left.fixed_columns::<1>(0), &b);
                    iso
                })
                .collect();
            Ok(SolveOut {
                stats: res,
                // stepper: stepper.clone(),
                time: stepper.x_out().to_vec(),
                frenet_serret,
                acc_aligned,
            })
        } //todo: avoid clone
        Err(_) => todo!(),
    }
    //todo: stop on length
}
