use nutype::nutype;

#[nutype(validate(with = |x| *x>0.0))]
#[derive(*)]
pub struct Length(f64);

pub struct NKeyPoint(pub usize);

#[nutype(validate(with = |x| *x>0.0))]
#[derive(*)]
pub struct Ds(f64);

#[nutype(validate(with = |x| *x>0.0))]
#[derive(*)]
pub struct MaxCurve(f64);

#[nutype(validate(with = |x| *x>0.0))]
#[derive(*)]
pub struct MaxTorsion(f64);

#[nutype(validate(with = |x| *x>0.0))]
#[derive(*)]
pub struct MaxDCurve(f64);

#[nutype(validate(with = |x| *x>0.0))]
#[derive(*)]
pub struct MaxDTorsion(f64);
