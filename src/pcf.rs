pub struct PiecewiseConstantFunction {
    breakpoints: Vec<f64>, //todo: check that it is sorted
    values: Vec<f64>,
}

impl PiecewiseConstantFunction {
    pub fn new(mut breakpoints: Vec<f64>, values: Vec<f64>) -> Self {
        breakpoints.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
        Self {
            breakpoints,
            values,
        }
    }
    pub fn eval(&self, x: f64) -> &f64 {
        let i = largest_smaller_index(self.breakpoints.as_slice(), x);
        &self.values[i.unwrap()]
    }
}

// todo: mod utility? could be rewritten better
fn largest_smaller_index(arr: &[f64], x: f64) -> Option<usize> {
    if x == arr[0] {
        return Some(0);
    }
    match arr.binary_search_by(|&y| y.partial_cmp(&x).unwrap()) {
        Ok(index) => {
            if index > 0 {
                Some(index - 1)
            } else {
                None
            }
        }
        Err(index) => {
            if index > 0 {
                Some(index - 1)
            } else {
                None
            }
        }
    }
}
