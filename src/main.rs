use frenet_serret::pcf::PiecewiseConstantFunction;
use frenet_serret::{new_init, solve, DopriParams, Dynamics, DynamicsParam, FS};
use std::f64::consts::PI;

fn main() {
    //doing a pure circle //todo: convert to test
    let length = 100.0;
    let r = length / 2.0 / PI;
    let vel = 2.0;
    let omega = vel / r;
    let curv = r * omega.powf(2.0);
    let dcurv = PiecewiseConstantFunction::new(vec![0.], vec![0.]);
    let dtors = PiecewiseConstantFunction::new(vec![0.], vec![0.]);
    let ds = PiecewiseConstantFunction::new(vec![0.], vec![0.]);
    let frenet_serret = FS::build(
        Dynamics {
            dcurv_ds: |s| *dcurv.eval(s),
            dtors_ds: |s| *dtors.eval(s),
            dvel_ds: |s| *ds.eval(s),
            param: DynamicsParam {
                max_curve: 1.,
                max_tors: 1.,
                max_vel: 10.,
                max_acc: 10.,
                max_length: 100.,
            },
        },
        new_init(curv, 0.0, vel),
        DopriParams {
            dt: 0.1,
            max_time: 100.,
            rtol: 0.0001,
            atol: 0.0001,
        },
    )
    .unwrap();
    let solution = solve(frenet_serret).unwrap();
    println!("{:#?}", solution.stats);
    println!("{:#?}", solution.time)
}
